#include "BOX.h"
#include "auxiliar.h"

#include <windows.h>

#include <gl/glew.h>
#define SOLVE_FGLUT_WARNING
#include <gl/freeglut.h>
#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

//////////////////////////////////////////////////////////////
// Datos que se almacenan en la memoria de la CPU
//////////////////////////////////////////////////////////////

// Matrices
glm::mat4 proj = glm::mat4(1.0f);
glm::mat4 view = glm::mat4(1.0f);
glm::mat4 model = glm::mat4(1.0f);

// Variables para los ejercicios
float yaw;
float pitch;
glm::vec3 eyeVec = glm::vec3(0);

//////////////////////////////////////////////////////////////
// Variables que nos dan acceso a Objetos OpenGL
//////////////////////////////////////////////////////////////
// Por definir
unsigned int vshader;
unsigned int fshader;
unsigned int program;
unsigned int vshader2;
unsigned int fshader2;
unsigned int program2;

// Variables Uniform
int uModelViewMat;
int uModelViewProjMat;
int uNormalMat;
// Variables Uniform 2
int uModelViewMat2;
int uModelViewProjMat2;
int uNormalMat2;

// Atributos
int inPos;
int inColor;
int inNormal;
int inTexCoord;
// Atributos 2
int inPos2;
int inColor2;
int inNormal2;
int inTexCoord2;

// Texturas
unsigned int colorTexId;
unsigned int emiTexId;
unsigned int colorTexId2;
unsigned int emiTexId2;

// Texturas Uniform
int uColorTex;
int uEmiTex;
// Texturas Uniform2
int uColorTex2;
int uEmiTex2;

// Ajustes de luz
unsigned int u_Ia_Value;
float Ia_Value;
unsigned int u_lpos_Value;
float lpos_Value[3];

// Mouse
float mouseX;
float mouseY;

// VAO
unsigned int vao;
// VBOs que forman parte del objeto
unsigned int posVBO;
unsigned int colorVBO;
unsigned int normalVBO;
unsigned int texCoordVBO;
unsigned int triangleIndexVBO;

//////////////////////////////////////////////////////////////
// Funciones auxiliares
//////////////////////////////////////////////////////////////

// Para los ejercicios
void UpdateView();

// Declaracion de CB
void renderFunc();
void resizeFunc(int width, int height);
void idleFunc();
void keyboardFunc(unsigned char key, int x, int y);
void mouseFunc(int button, int state, int x, int y);
void mouseMotionFunc(int x, int y);

// Funciones de inicializaci�n y destrucci�n
void initContext(int argc, char **argv);
void initOGL();
void initShader(const char *vname, const char *fname, const char *vname2,
	const char *fname2);
void initObj();
void destroy();

// Carga el shader indicado, devuele el ID del shader
GLuint loadShader(const char *fileName, GLenum type);
// Crea una textura, la configura, la sube a OpenGL, y devuelve el identificador
// de la textura
unsigned int loadTex(const char *fileName);

int main(int argc, char **argv) {
	std::locale::global(std::locale("spanish")); // acentos ;)

	initContext(argc, argv);
	initOGL();
	initShader("../shaders_P3/shader.v1.vert", "../shaders_P3/shader.v1.frag",
		"../shaders_P3/shader.v0.vert", "../shaders_P3/shader.v0.frag");
	initObj();

	glutMainLoop();
	destroy();
	return 0;
}

//////////////////////////////////////////
// Funciones auxiliares
void initContext(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitContextVersion(4, 2);
	glutInitContextProfile(GLUT_CORE_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Practicas GLSL");

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) {
		std::cout << "Error: " << glewGetErrorString(err) << std::endl;
		exit(-1);
	}

	const GLubyte *oglVersion = glGetString(GL_VERSION);
	std::cout << "This system supports OpenGL Version: " << oglVersion
		<< std::endl;

	glutReshapeFunc(resizeFunc);
	glutDisplayFunc(renderFunc);
	glutIdleFunc(idleFunc);
	glutKeyboardFunc(keyboardFunc);
	glutMouseFunc(mouseFunc);
	glutMotionFunc(mouseMotionFunc);
}

void initOGL() {
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.2f, 0.2f, 0.2f, 0.0f);

	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_CULL_FACE);

	proj = glm::perspective(glm::radians(60.0f), 1.0f, 0.1f, 50.0f);
	view = glm::mat4(1.0f);
	view[3].z = -6;
}
void destroy() {
	glDetachShader(program, vshader);
	glDetachShader(program, fshader);
	glDeleteShader(vshader);
	glDeleteShader(fshader);
	glDeleteProgram(program);

	glDetachShader(program2, vshader2);
	glDetachShader(program2, fshader2);
	glDeleteShader(vshader2);
	glDeleteShader(fshader2);
	glDeleteProgram(program2);

	if (inPos != -1)
		glDeleteBuffers(1, &posVBO);
	if (inColor != -1)
		glDeleteBuffers(1, &colorVBO);
	if (inNormal != -1)
		glDeleteBuffers(1, &normalVBO);
	if (inTexCoord != -1)
		glDeleteBuffers(1, &texCoordVBO);
	glDeleteBuffers(1, &triangleIndexVBO);
	glDeleteVertexArrays(1, &vao);

	glDeleteTextures(1, &colorTexId);
	glDeleteTextures(1, &emiTexId);
}

void initShader(const char *vname, const char *fname, const char *vname2,
	const char *fname2) {
	// Program 1
	vshader = loadShader(vname, GL_VERTEX_SHADER);
	fshader = loadShader(fname, GL_FRAGMENT_SHADER);

	program = glCreateProgram();
	glAttachShader(program, vshader);
	glAttachShader(program, fshader);

	glBindAttribLocation(program, 0, "inPos");
	glBindAttribLocation(program, 1, "inColor");
	glBindAttribLocation(program, 2, "inNormal");
	glBindAttribLocation(program, 3, "inTexCoord");

	glLinkProgram(program);

	int linked;
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if (!linked) {
		// Calculamos una cadena de error
		GLint logLen;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetProgramInfoLog(program, logLen, NULL, logString);
		std::cout << "Error: " << logString << std::endl;
		delete[] logString;
		exit(-1);
	}

	uNormalMat = glGetUniformLocation(program, "normal");
	uModelViewMat = glGetUniformLocation(program, "modelView");
	uModelViewProjMat = glGetUniformLocation(program, "modelViewProj");

	inPos = glGetAttribLocation(program, "inPos");
	inColor = glGetAttribLocation(program, "inColor");
	inNormal = glGetAttribLocation(program, "inNormal");
	inTexCoord = glGetAttribLocation(program, "inTexCoord");

	uColorTex = glGetUniformLocation(program, "colorTex");
	uEmiTex = glGetUniformLocation(program, "emiTex");
	u_Ia_Value = glGetUniformLocation(program, "Ia_Value");
	u_lpos_Value = glGetUniformLocation(program, "lpos_Value");

	// Program 2
	vshader2 = loadShader(vname2, GL_VERTEX_SHADER);
	fshader2 = loadShader(fname2, GL_FRAGMENT_SHADER);

	program2 = glCreateProgram();
	glAttachShader(program2, vshader2);
	glAttachShader(program2, fshader2);

	glBindAttribLocation(program2, 0, "inPos");
	glBindAttribLocation(program2, 1, "inColor");
	glBindAttribLocation(program2, 2, "inNormal");
	glBindAttribLocation(program2, 3, "inTexCoord");

	glLinkProgram(program2);

	int linked2;
	glGetProgramiv(program2, GL_LINK_STATUS, &linked2);
	if (!linked2) {
		// Calculamos una cadena de error
		GLint logLen;
		glGetProgramiv(program2, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetProgramInfoLog(program2, logLen, NULL, logString);
		std::cout << "Error: " << logString << std::endl;
		delete[] logString;
		exit(-1);
	}

	uNormalMat2 = glGetUniformLocation(program2, "normal");
	uModelViewMat2 = glGetUniformLocation(program2, "modelView");
	uModelViewProjMat2 = glGetUniformLocation(program2, "modelViewProj");

	inPos2 = glGetAttribLocation(program2, "inPos");
	inColor2 = glGetAttribLocation(program2, "inColor");
	inNormal2 = glGetAttribLocation(program2, "inNormal");
	inTexCoord2 = glGetAttribLocation(program2, "inTexCoord");

	uColorTex2 = glGetUniformLocation(program2, "colorTex");
	uEmiTex2 = glGetUniformLocation(program2, "emiTex");
}

void initObj() {
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Program 1
	if (inPos != -1) {
		glGenBuffers(1, &posVBO);
		glBindBuffer(GL_ARRAY_BUFFER, posVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex * sizeof(float) * 3,
			cubeVertexPos, GL_STATIC_DRAW);
		glVertexAttribPointer(inPos, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(inPos);
	}
	if (inColor != -1) {
		glGenBuffers(1, &colorVBO);
		glBindBuffer(GL_ARRAY_BUFFER, colorVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex * sizeof(float) * 3,
			cubeVertexColor, GL_STATIC_DRAW);
		glVertexAttribPointer(inColor, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(inColor);
	}
	if (inNormal != -1) {
		glGenBuffers(1, &normalVBO);
		glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex * sizeof(float) * 3,
			cubeVertexNormal, GL_STATIC_DRAW);
		glVertexAttribPointer(inNormal, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(inNormal);
	}
	if (inTexCoord != -1) {
		glGenBuffers(1, &texCoordVBO);
		glBindBuffer(GL_ARRAY_BUFFER, texCoordVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex * sizeof(float) * 2,
			cubeVertexTexCoord, GL_STATIC_DRAW);
		glVertexAttribPointer(inTexCoord, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(inTexCoord);
	}

	// Program 2
	if (inPos2 != -1) {
		glGenBuffers(1, &posVBO);
		glBindBuffer(GL_ARRAY_BUFFER, posVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex * sizeof(float) * 3,
			cubeVertexPos, GL_STATIC_DRAW);
		glVertexAttribPointer(inPos2, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(inPos2);
	}
	if (inColor2 != -1) {
		glGenBuffers(1, &colorVBO);
		glBindBuffer(GL_ARRAY_BUFFER, colorVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex * sizeof(float) * 3,
			cubeVertexColor, GL_STATIC_DRAW);
		glVertexAttribPointer(inColor2, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(inColor2);
	}
	if (inNormal2 != -1) {
		glGenBuffers(1, &normalVBO);
		glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex * sizeof(float) * 3,
			cubeVertexNormal, GL_STATIC_DRAW);
		glVertexAttribPointer(inNormal2, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(inNormal2);
	}
	if (inTexCoord2 != -1) {
		glGenBuffers(1, &texCoordVBO);
		glBindBuffer(GL_ARRAY_BUFFER, texCoordVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex * sizeof(float) * 2,
			cubeVertexTexCoord, GL_STATIC_DRAW);
		glVertexAttribPointer(inTexCoord2, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(inTexCoord2);
	}

	glGenBuffers(1, &triangleIndexVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleIndexVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		cubeNTriangleIndex * sizeof(unsigned int) * 3, cubeTriangleIndex,
		GL_STATIC_DRAW);

	model = glm::mat4(1.0f);
	colorTexId = loadTex("../img/color2.png");
	emiTexId = loadTex("../img/emissive.png");
}

GLuint loadShader(const char *fileName, GLenum type) {
	unsigned int fileLen;
	char *source = loadStringFromFile(fileName, fileLen);

	// Creacion y compilacion del Shader
	GLuint shader;
	shader = glCreateShader(type);
	glShaderSource(shader, 1, (const GLchar **)&source, (const GLint *)&fileLen);
	glCompileShader(shader);
	delete[] source;

	// Comprobamos que se compila bien
	GLint compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		// Calculamos una cadena de error
		GLint logLen;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetShaderInfoLog(shader, logLen, NULL, logString);
		std::cout << "Error: " << logString << std::endl;
		delete[] logString;
		glDeleteShader(shader);
		exit(-1);
	}

	return shader;
}

unsigned int loadTex(const char *fileName) {
	unsigned char *map;
	unsigned int w, h;

	map = loadTexture(fileName, w, h);
	if (!map) {
		std::cout << "Error cargando el fichero: " << fileName << std::endl;
		exit(-1);
	}

	unsigned int texId;
	glGenTextures(1, &texId);
	glBindTexture(GL_TEXTURE_2D, texId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE,
		(GLvoid *)map);
	delete[] map;

	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	return texId;
}

void renderFunc() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// OBJ 1
	glUseProgram(program);
	glm::mat4 modelView = view * model;
	glm::mat4 modelViewProj = proj * view * model;
	glm::mat4 normal = glm::transpose(glm::inverse(modelView));
	if (uModelViewMat != -1)
		glUniformMatrix4fv(uModelViewMat, 1, GL_FALSE, &(modelView[0][0]));
	if (uModelViewProjMat != -1)
		glUniformMatrix4fv(uModelViewProjMat, 1, GL_FALSE, &(modelViewProj[0][0]));
	if (uNormalMat != -1)
		glUniformMatrix4fv(uNormalMat, 1, GL_FALSE, &(normal[0][0]));

	// Texturas
	if (uColorTex != -1) {
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, colorTexId);
		glUniform1i(uColorTex, 0);
	}
	if (uEmiTex != -1) {
		glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, emiTexId);
		glUniform1i(uEmiTex, 1);
	}

	// intensidad de luz
	if (u_Ia_Value != -1)
		glProgramUniform1f(program, u_Ia_Value, Ia_Value);
	// posicion de luz
	if (u_lpos_Value != -1)
		glProgramUniform3fv(program, u_lpos_Value, 1, lpos_Value);

	glDrawElements(GL_TRIANGLES, cubeNTriangleIndex * 3, GL_UNSIGNED_INT, (void *)0);
	// end OBJ 1


	// OBJ 2
	glUseProgram(program2);
	glm::mat4 m2(1.0f);
	m2[3].x = 3.0f;
	modelView = view * model * m2;
	modelViewProj = proj * view * model * m2;
	normal = glm::transpose(glm::inverse(modelView));
	if (uModelViewMat2 != -1)
		glUniformMatrix4fv(uModelViewMat2, 1, GL_FALSE, &(modelView[0][0]));
	if (uModelViewProjMat2 != -1)
		glUniformMatrix4fv(uModelViewProjMat2, 1, GL_FALSE, &(modelViewProj[0][0]));
	if (uNormalMat2 != -1)
		glUniformMatrix4fv(uNormalMat2, 1, GL_FALSE, &(normal[0][0]));

	// Texturas
	if (uColorTex2 != -1) {
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, colorTexId);
		glUniform1i(uColorTex2, 0);
	}
	if (uEmiTex2 != -1) {
		glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, emiTexId);
		glUniform1i(uEmiTex2, 1);
	}

	glDrawElements(GL_TRIANGLES, cubeNTriangleIndex * 3, GL_UNSIGNED_INT, (void *)0);
	// end OBJ 2

	glBindVertexArray(vao);
	glutSwapBuffers();
}

void resizeFunc(int width, int height) {
	glViewport(0, 0, width, height);
	float ratio = (float)width / (float)height;
	proj = glm::perspective(glm::radians(60.0f), ratio, 0.1f, 50.0f);
}

void idleFunc() {
	model = glm::mat4(1.0f);
	static float angle = 0.0f;
	angle = (angle > 3.141592f * 2.0f) ? 0 : angle + 0.01f;
	model = glm::rotate(model, angle, glm::vec3(1.0f, 1.0f, 0.0f));
	glutPostRedisplay();
}

void keyboardFunc(unsigned char key, int x, int y) {
	std::cout << "KEY: " << key << std::endl << std::endl;
	const float speed = 0.15f; // how fast we move
	float dx = 0.0f;           // how much we strafe on X
	float dz = 0.0f;           // how much we walk on Z

	// Evaluate input key - [ P1.3 ]
	switch (key) {

		// Movement

	case 'w':
		dz = -speed;
		break;
	case 's':
		dz = speed;
		break;
	case 'a':
		dx = -speed;
		break;
	case 'd':
		dx = speed;
		break;

		// Camera

	case 'l':
		pitch += speed / 2.0f;
		break;
	case 'j':
		pitch -= speed / 2.0f;
		break;
	case 'k':
		yaw += speed / 2.0f;
		break;
	case 'i':
		yaw -= speed / 2.0f;
		break;

		// Light intensity

	case '+':
		Ia_Value += 0.1f;
		break;
	case '-':
		Ia_Value -= 0.1f;
		break;

		// Light position

	case '8':
		lpos_Value[1] += 0.1f;
		break;
	case '2':
		lpos_Value[1] -= 0.1f;
		break;
	case '6':
		lpos_Value[0] += 0.1f;
		break;
	case '4':
		lpos_Value[0] -= 0.1f;
		break;
	case '5':
		lpos_Value[2] += 0.1f;
		break;
	case '0':
		lpos_Value[2] -= 0.1f;
		break;

	default:
		break;
	}

	glm::vec3 forward(view[0][2], view[1][2], view[2][2]); // Z
	glm::vec3 strafe(view[0][0], view[1][0], view[2][0]);  // X
	eyeVec += (dz * forward + dx * strafe);
	UpdateView();
}

// MOUSE BUTTONS INPUT ACTIONS
void mouseFunc(int button, int state, int x, int y) {
  if (state == 0) {
    std::cout << "MOUSE BUTTON PRESSED: ";
  } else {
    std::cout << "MOUSE BUTTON RELEASED: ";
  }
  if (button == 0)
    std::cout << "Left" << std::endl;
  if (button == 1)
    std::cout << "Center" << std::endl;
  if (button == 2)
    std::cout << "Right" << std::endl;
  std::cout << "AT (" << x << ", " << y << ")" << std::endl << std::endl;

  if (state == 0 && button == 0) // left click enter
  {
    mouseX = (float)x;
    mouseY = (float)y;
  }
}

// MOUSE DISPLACEMENT INPUT ACTIONS - [ P1.O1 ]
void mouseMotionFunc(int x, int y) {
  pitch += (mouseX - (float)x) * 0.0001f;
  yaw += (mouseY - (float)y) * 0.0001f;
  UpdateView();
}

// UPDATE CORRECTLY - Fix camera bug of lose Z axis
void UpdateView() {
	glm::mat4 matYaw = glm::mat4(1.0f);   // identity matrix
	glm::mat4 matPitch = glm::mat4(1.0f); // identity matrix

	// Apply acumulated angles
	matYaw = glm::rotate(matYaw, yaw, glm::vec3(-1.0f, 0.0f, 0.0f));
	matPitch = glm::rotate(matPitch, pitch, glm::vec3(0.0f, 1.0f, 0.0f));

	// Calc roration
	glm::mat4 rotate = glm::mat4(1.0f);
	rotate = matYaw * matPitch;

	// Calc translation
	glm::mat4 translate = glm::mat4(1.0f);
	translate[3].z = -6;
	translate = glm::translate(translate, -eyeVec);

	// New view will be the product of two previous matrix
	view = rotate * translate;
}
