#version 330 core

in vec3 inPos;		

void main()
{
	mat4 view = mat4(1.0);
	mat4 proj = mat4(0.0);

	float toRad = 3.14159 / 180.0;

	float near = 1.0;
	float far = 10.0;

	view[3] = vec4(0.0, 0.0, -10.0, 1.0);

	proj[0].x = 1.0/tan(30*toRad);
	proj[1].y = proj[0].x;
	proj[2].z = -(far+near) / (far-near);
	proj[3].z = -2*near*far / (far-near);
	proj[2].w = -1.0;
	
	gl_Position =  proj * view * vec4(inPos,1.0);
}
