#version 330 core
in vec3 inPos;
in vec3 inNormal;
in vec2 inTexCoord;
uniform mat4 modelViewProj;
uniform mat4 modelView;
uniform mat4 normal;
out vec3 norm;
out vec2 texCoord;
out vec3 position;
void main()
{
	norm = (normal * vec4(inNormal,0)).xyz;
	position = (modelView * vec4(inPos,1)).xyz;
	texCoord = inTexCoord;
	gl_Position =  modelViewProj * vec4 (inPos,1);
}
