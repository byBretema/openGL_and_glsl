#version 330 core
out vec3 color;
in vec3 inPos;
in vec3 inColor;
in vec3 inNormal;
uniform mat4 normal;
uniform mat4 modelView;
uniform mat4 modelViewProj;
vec3 Il = vec3(1);
vec3 pl = vec3(0);
vec3 Ia = vec3(0.15);
vec3 N;
vec3 pos;
float n = 5;
vec3 Ka = vec3(1,0,0);
vec3 Kd = vec3(1,0,0);
vec3 Ks = vec3(1);
vec3 shade()
{
	vec3 c = vec3(0);
	c += Ia * Ka;
	vec3 L = normalize(pl-pos);
	c += clamp(Il * Kd * dot(N,L), 0, 1);
	vec3 R = reflect(-L,N);
	vec3 V = normalize(-pos);
	float spcFactor = pow(max(dot(R,V),0.00001),n);
	c += Il * Ks * spcFactor;
	return c;
}
void main()
{
	N = (normal * vec4(inNormal,0)).xyz;
	pos = (modelView * vec4(inPos,1)).xyz;
	Ka = inColor;
	Kd = inColor;
	color = shade();
	gl_Position =  modelViewProj * vec4 (inPos,1);
}
