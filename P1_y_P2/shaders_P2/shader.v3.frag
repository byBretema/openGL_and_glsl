#version 330 core

uniform sampler2D colorTex;
uniform sampler2D specularTex;
uniform sampler2D emiTex;

in vec3 norm;
in vec3 position;
in vec3 color;
in vec2 texCoord;

out vec4 outColor;

/*** Harcoded light settings *******************/
// SCENE
vec3 Ia = vec3(0.05); // Ambiental instensity
// LIGHT 1
vec3 Il_1 = vec3(2); // Light intensity
vec3 pl_1 = vec3(0,3,0); // Light pos
vec3 color_1 = vec3(1,0,0);
// LIGHT 2
vec3 Il_2 = vec3(2);  // Light intensity
vec3 pl_2 = vec3(0,-3,0); // Light pos
vec3 color_2 = vec3(0,0,1);
// OBJECTS
vec3 N;					 // Normal
vec3 pos;				 // Obj pos
float n = 5;             // Bright
vec3 Ka = vec3(1,0,0);   // Ambiental
vec3 Kd = vec3(1,0,0);   // Difusse
vec3 Ks = vec3(1);       // Specular
vec3 Ke = vec3(0);
/************************************************/

// Specular factor of a: Light direction vector
float specFactor(vec3 L){
	vec3 R = reflect(-L,N);
	vec3 V = normalize(-pos);
	return pow(max(dot(R,V),0.00001),n);
}

// Calc one light
vec3 pointLight(vec3 intensity, vec3 lightPos, vec3 color){
	vec3 light = vec3(0);
	vec3 L = normalize(lightPos - pos);
	float d = length(L);
	// [ P2.2 ]
	light += color * clamp(intensity * Kd * dot(N,L), 0, 1);
	light += color * intensity * Ks * specFactor(L);
	// End [ P2.2 ]
	return light;
}

// Calc all lights
vec3 shade() {
	vec3 c = vec3(0);
	// LIGHT 1 - Up
	c += pointLight(Il_1, pl_1, color_1);
	// LIGHT 2 - Down - [ P2.1 ]
	c += pointLight(Il_2, pl_2, color_2);
	// OTHER LIGHTS
	c += Ia * Ka; 	// Add ambiental
	c += Ke; 		// 'Auxiliar' for emisive texture
	return c;
}


// --- SHADER ENTRY POINT ---

void main()
{
	/*** Caputre data ***********************************/
	pos = position;
	N = normalize(norm); // Make normals unitary again.
	Ka = texture(colorTex,texCoord).rgb;
	Kd = Ka;
	Ks = texture(specularTex,texCoord).rgb;
	Ke = texture(emiTex,texCoord).rgb;
	/****************************************************/

	/*** Calc light ***************/
	vec3 I = shade();
	// fog - [ P2.O2 ]
	vec3 cf = vec3(0);
	float d = length(pos);
	float alfa = 1/exp(0.02*d*d);
	vec3 c = alfa*I+(1-alfa)*cf;
	/*****************************/

	/*** Assign output data ****/
	outColor = vec4(c,1.0);
	/**************************/
}
