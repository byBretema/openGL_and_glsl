#version 330 core

//Color de salida
out vec4 outColor;

//Variables Variantes
in vec2 texCoord;

//Textura
uniform sampler2D colorTex;
uniform sampler2D vertexTex;

uniform bool dofByCam;
uniform float focalDistance;
uniform float maxDistanceFactor;

// Igual que en el lado de cliente (deberían enviarse, no escribirse aqui)
uniform float near = 1.0;
uniform float far = 50.0;

#define MASK_SIZE 25u
uniform float[MASK_SIZE] mask;
const vec2 texIdx[MASK_SIZE] = vec2[](
vec2(-2.0,2.0), vec2(-1.0,2.0), vec2(0.0,2.0), vec2(1.0,2.0), vec2(2.0,2.0),
vec2(-2.0,1.0), vec2(-1.0,1.0), vec2(0.0,1.0), vec2(1.0,1.0), vec2(2.0,1.0),
vec2(-2.0,0.0), vec2(-1.0,0.0), vec2(0.0,0.0), vec2(1.0,0.0), vec2(2.0,0.0),
vec2(-2.0,-1.0), vec2(-1.0,-1.0), vec2(0.0,-1.0), vec2(1.0,-1.0), vec2(2.0,-1.0),
vec2(-2.0,-2.0), vec2(-1.0,-2.0), vec2(0.0,-2.0), vec2(1.0,-2.0), vec2(2.0,-2.0));


float dofKb(float fDist) {
	float dof = abs(texture(vertexTex,texCoord).z - fDist) * maxDistanceFactor;
	dof = clamp (dof, 0.0, 1.0);
	dof *= dof;
	return dof;
}

float dofCam() {
	float zCam =  near*far / (far + (texture(vertexTex,texCoord).z * (near-far)));
	return dofKb(zCam);
}

void main()
{
	//Ser�a m�s r�pido utilizar una variable uniform el tama�o de la textura.
	vec2 ts = vec2(1.0) / vec2 (textureSize (colorTex,0));

	float dof = 0.0;
	if (dofByCam){
		dof = dofCam();
	} else {
		dof = dofKb(focalDistance);
	}

	vec4 color = vec4 (0.0);

	for (uint i = 0u; i < MASK_SIZE; i++)
	{
		vec2 iidx = texCoord + ts * texIdx[i]*dof;
		color += texture(colorTex, iidx,0.0) * mask[i];
	}
outColor = color;
}
